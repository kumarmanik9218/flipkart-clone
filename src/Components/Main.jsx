import React from 'react'
// import Header from './header'
import Product from './Product'
import Crausel from './Crausel'
import Footer from './Footer'
import { Link } from 'react-router-dom'

export default function Main() {
  return (
    <div>
         {/* <Header/> */}
      <Crausel/>
     <Link to={'/ProductDetail'}> <Product/>  </Link>
      <Footer/>
    </div>
  )
}
