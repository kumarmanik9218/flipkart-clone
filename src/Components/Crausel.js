import React from 'react'
import { Carousel } from 'react-bootstrap'

function Crausel() {
    return (
        <div className='crsl'>

            <div>
                <Carousel>
                    <Carousel.Item style={{ 'height': "300px" }}>
                        <img style={{ 'height': "300px" }}
                            className="d-block w-100"
                            src="https://i.pinimg.com/originals/37/e9/e0/37e9e012b6e0f2a3edf47438f066958d.jpg" alt="" />

                    </Carousel.Item>
                    <Carousel.Item style={{ 'height': "300px" }}>
                        <img style={{ 'height': "300px" }}
                            className="d-block w-100"
                            src="https://s3-eu-west-1.amazonaws.com/cdn1.mullenlowegroup.com/uploads/2017/09/News-Carousel-Lintas-Bangalore-Flipkart.jpg" alt="" />
                    </Carousel.Item>


                    <Carousel.Item style={{ 'height': "300px" }}>
                        <img style={{ 'height': "300px" }}
                            className="d-block w-100"
                            src="https://miro.medium.com/max/1400/1*JiQ5iC50TCDtTKSy713oZA.png" alt="" />
                    </Carousel.Item>

                    <Carousel.Item style={{ 'height': "300px" }}>
                        <img style={{ 'height': "300px" }}
                            className="d-block w-100"
                            src="https://i.gadgets360cdn.com/large/flipkart_main_1576927335243.jpg" alt="" />
                    </Carousel.Item>
                </Carousel>
            </div>
        </div>
    )
}
export default Crausel;
