// import React from 'react'
// import { Navbar } from 'react-bootstrap'
// import { Button } from 'react-bootstrap'
// import { Nav } from 'react-bootstrap'
// import { Form } from 'react-bootstrap'
// import { Col } from 'react-bootstrap'
// import { Container } from 'react-bootstrap'
// // import {NavDropdown} from 'react-bootstrap'
// import { FormControl } from 'react-bootstrap'

import React from "react";
import {AppBar, Toolbar,CssBaseline,Typography,makeStyles}  from "@material-ui/core";
import {Link} from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  navlinks: {
    marginLeft: theme.spacing(10),
    display: "flex",
  },
 logo: {
    flexGrow: "1",
    cursor: "pointer",
  },
  link: {
    textDecoration: "none",
    color: "white",
    fontSize: "20px",
    marginLeft: theme.spacing(20),
    "&:hover": {
      color: "yellow",
      borderBottom: "1px solid white",
    },
  },
}));


function Header() {
  const classes = useStyles();
  return (
    
    <div>
      <AppBar position="static">
        <CssBaseline/>
        <Toolbar>
          <Typography variant="h4" className={classes.logo}>
            FlipKart
          </Typography>
          <div className={classes.navlinks}>
              <Link to="/" className={classes.link}>
                Home
              </Link>
              <Link to="/about" className={classes.link}>
                About
              </Link>
              <Link to="/contact" className={classes.link}>
                Contact
              </Link>
              <Link to="/login" className={classes.link}>
                LogIn
              </Link>
          </div>
        </Toolbar>
      </AppBar>
    </div>

  );
}


export default Header;
