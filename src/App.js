import React from 'react';
import './App.css';
import Header from './Components/header';
// import Crausel from './Components/Crausel';
// import Product from './Components/Product';
// import Footer from './Components/Footer';
import ProductDetail from './Page/ProductDetail';
import Main from './Components/Main';
import Login from './Page/Login';
import {

  Route,
  BrowserRouter as Router,
  

} from "react-router-dom";
import { Link, Switch } from "react-router";

function App() {
  return (
    <div className="App">

      <Router>
        <Header/>
        <Switch>
          <Route exact path="/" component={Main} />
          <Route exact path="/ProductDetail" component={ProductDetail} />
          <Route exact path="/login" component={Login} />
        </Switch>
      </Router>

    </div>

  );
}

export default App;
