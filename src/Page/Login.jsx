import React from 'react'
import './login.css';
import {Button} from 'react-bootstrap';

function Login() {
  return (
    <div>
        <h2>Login Page</h2>

        <form action="/action_page.php" method="post">
  <div className="imgcontainer">
    <img src="img_avatar2.png" alt="Avatar" className="avatar"/>
  </div>

  <div className="container">
    <label for="uname"><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="uname" required/>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" required/>
        
    <Button type="submit">Login</Button>
    <label>
      <input type="checkbox" checked="checked" name="remember"/> Remember me
    </label>
  </div>

  <div className="container" style={{backgroundColor:"#f1f1f1"}}>
    <Button type="button" className="cancelbtn">Cancel</Button>
    <span className="psw">Forgot <a href="#">password?</a></span>
  </div>
</form>

    </div>
  )
}

export default Login;