import React from 'react'
import './ProductDetail.css'
import {Button} from 'react-bootstrap';

function ProductDetail() {
    return (
        <div>
            <div className="container-fluid">
                <div className="row justify-content-around">
                    <div className="col-md-4">
                        <div className="card">
                            <img src="https://rukminim2.flixcart.com/image/612/612/klqx30w0/deodorant/y/z/w/240-fresh-fougere-fresh-fougere-240ml-2-body-spray-fogg-men-original-imagytyffgkepkfd.jpeg?q=70" alt='Error' />
                        </div>
                        <Button mat-raised-Button> <i className="fa fa-shopping-cart " style={{ fontSize: "25px" }}></i> &nbsp; ADD TO CART</Button>
                        <Button mat-raised-Button style={{ backgroundColor: "darkslateblue" }}>BUY</Button>
                    </div>
                    <div className="col-md-7 mt-3">
                        <h2>AXE Dark Temptation & Gold Temptation Long Lasting For Men (Pack of 2) Body Spray-For
                            Men (215 ml,Pack of 2)
                        </h2>
                        <span className="star">4<i className="fa fa-star" aria-hidden="true"></i></span>
                        <span className="pr-3">1,007 Rating & 72 Reviews</span>
                        <h3 className="mt-3" style={{ Color: "success" }}>Special Price</h3>
                        <div>
                            <h1 style={{ fontWeight: "bold" }} className="price">260</h1>
                            <p style={{ textDecoration: "line-through" }} className="price">560</p>
                            <h3 style={{ color: "success" }} className="price">56 % off</h3>
                            <p className="price">@120.9/100ml</p>
                        </div>
                        <h4 style={{ fontWeight: "bold" }}>Coupons For You</h4>
                        <img src="https://rukminim2.flixcart.com/www/36/36/promos/30/07/2019/79f48e86-8a93-46ab-b45a-5a12df491941.png?q=90"
                            width="18" height="18" className="_3HLfAg" alt='error' />
                        <span style={{ fontWeight: "bold" }} className="ml-3">Special Price</span>
                        <span className="ml-5">Get Extra 20% off upto 100 on 1 item(s)</span>
                        <h4 style={{ fontWeight: "bold" }} className="mt-3">Available Offers</h4>
                        <img src="https://rukminim2.flixcart.com/www/36/36/promos/06/09/2016/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png?q=90"
                            width="18" height="18" className="_3HLfAg" alt='Error' />
                        <span style={{ fontWeight: "bold" }} className="ml-3">Special Price</span>
                        <span className="ml-5">Get Extra 27% off (price inclusive of discount)</span>
                        <br />
                        <img src="https://rukminim2.flixcart.com/www/36/36/promos/06/09/2016/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png?q=90"
                            width="18" height="18" className="_3HLfAg" alt='error' />
                        <span className="ml-3" style={{ fontWeight: "bold" }} >Combo Offer</span>
                        <span className="ml-5">Buy 3 item and save 5%,Bur 4 save 7%,Buy 5+ save 10%</span>
                        <br />
                        <img src="https://rukminim2.flixcart.com/www/36/36/promos/06/09/2016/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png?q=90"
                            width="18" height="18" className="_3HLfAg" alt='Error' />
                        <span style={{ fontWeight: "bold" }} className="ml-3">Bank Offers</span>
                        <span className="ml-5">5% uUnlimited Cashback on Flipcart Axis Bank Credit Card</span>
                        <div className="row justify-content-around mt-4">
                            <div className="col-md-3">
                                <h3>Delivery</h3>
                            </div>
                            <div className="col-md-8">
                                <span style={{ textDecoration: "underline" }}><i className="fa fa-map-marker" aria-hidden="true"></i> Enter Delivery Pincode</span>
                                <span className="ml-3" style={{ color: "primary" }}>Check</span>
                                <h3 style={{ fontWeight: "bold" }}>Usually delivered in 7 days</h3>
                                <p>Enter Pincode for exact delivery dates/charges</p>
                                <h3 style={{ fontWeight: "bold", color: "blue" }}>View Details</h3>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProductDetail